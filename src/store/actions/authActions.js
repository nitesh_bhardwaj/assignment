import * as types from '../constants/actionTypes'

export const login = (isLoggedIn, user) => ({type: types.LOGIN, isLoggedIn, user})
export const logout = () => ({type: types.LOGOUT})
