import * as types from '../constants/actionTypes'

export const SEARCH = (userName) => ({type: types.SEARCH, userName})
