import {LOGIN, LOGOUT} from '../constants/actionTypes'
import Storage from '../../services/Storage'


export default function loader(state=Storage.getAuth(), action) {
  switch (action.type) {
    case LOGIN :
      let {isLoggedIn, user} = action;
      Storage.saveAuth({isLoggedIn, user})
      return {isLoggedIn, user};
    case LOGOUT:
      Storage.logout();
      return {isLoggedIn: false, user: null};
    default:
      return state;
  }
}
