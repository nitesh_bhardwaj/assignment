import {SEARCH} from '../constants/actionTypes'
import Storage from '../../services/Storage'
import {checkTimePass} from '../../utils/common'

export default function loader(state=Storage.get("searchUser"), action) {
  switch (action.type) {
    case SEARCH :
      let {userName} = action;
      let userState = state[userName] || {};

      if(!userState.time || checkTimePass(userState.time)) {
        userState.time = new Date();
        userState.count = 1;
      } else {
        userState.count += 1;  
      }

      let searchUsers = {
        ...state,
        [userName]: userState
      }
      Storage.set("searchUser", searchUsers);
      return searchUsers;
    default:
      return state;
  }
}
