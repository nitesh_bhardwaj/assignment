import React, { Component } from 'react';


class Header extends Component {
  constructor(props) {
    super(props)
    
  }
  render() {
    return (
      <header className="header"> 
      	<p className="name"> {this.props.user.name} </p>
      	<p className="round-btn" onClick={_ => this.props.logout()}> Logout </p>
      </header>
    );
  }
}

export default Header;
