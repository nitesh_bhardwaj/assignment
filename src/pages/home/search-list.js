import React, { Component } from 'react';


class SearchList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: []
    }
  }
  searchList(e) {
    let {target} = e;
    let {value} = target;
    console.log(value);
  }
  render() {
  	let {list} = this.props;
    return (
      <div className="search-list"> 
        {
          list.map(planet => (
            <div className="search-box" key={planet.name}>
              <p className="bold"> {planet.name} </p>
              <p className="pop-box"> <span> Poplulation : </span> {planet.population} </p>
            </div>
          ))
        }
      </div>
    );
  }
}

export default SearchList;
