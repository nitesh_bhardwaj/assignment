import React, { Component } from 'react';
import { connect } from 'react-redux';
import SearchList from './search-list'
import swapiModule from '../../utils/swapiModule'
import {SEARCH} from '../../store/actions/searchActions'
import {checkTimePass} from '../../utils/common'

class SearchPlanet extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: [],
      processing: false,
      message: null
    }
    this.planets = null;
  }

  getPlanets(cb) {
    if(this.planets) return cb(this.planets);
    this.setState({processing: true});
    swapiModule.getPlanets((res) => {
      this.planets = res.results;
      cb(this.planets);
    })
  }

  filterList(list, key) {
    return list.filter((planet) => (
      planet.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
    ))
  }

  filterPlanetList(val) {
    if(this.state.processing) return;
    this.props.dispatch(SEARCH(this.props.user.name))
    if(this.checkFilterCount()) return;
    this.getPlanets((res) => {
      let message = null;
      let list = this.filterList(res, val);
      if(!list.length) message = 'not found';
      this.setState({list, processing: false, message});
    })
  }

  checkFilterCount() {
    let {searchCount, user} = this.props;
    if(searchCount > 15 && user.name !== "Luke Skywalker") {
      this.setState({message: 'you cannot search more than 15 searches in a minute ', list:[]})
      return true;
    }
    return false;
  }

  searchList(e) {
    let {target} = e;
    let {value} = target;
    if(!value) return this.setState({list: []})
    this.filterPlanetList(value);
  }
  render() {
  	let {list, processing, message} = this.state;
    return (
      <div className="flex-center">
        <div className="search-cont"> 
          <input type="text" name="search" onChange={this.searchList.bind(this)} placeholder="search planet"/>
          {message && <p> {message} </p>}  
          <SearchList list={list} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  let {user} = state.auth;
  let search = state.search[user.name];
  return { 
   user,
   searchCount: search? search.count : 0,
   searchTime: search? search.time : null
  }
}

export default connect(mapStateToProps, null)(SearchPlanet);
