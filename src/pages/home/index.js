import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from './header'
import SearchPlanet from './search-planet';
import {logout} from '../../store/actions/authActions' 

const mapStateToProps = (state) => ({
	user: state.auth.user
})

class Home extends Component {
  
  logout() {
  	this.props.dispatch(logout());
  }

  render() {
  	console.log(this.props)
  	let {user} = this.props;
    return (
    <div>
      <Header user={user} logout={this.logout.bind(this)} />
      <SearchPlanet />
    </div>
    );
  }
}

export default connect(mapStateToProps, null)(Home);
