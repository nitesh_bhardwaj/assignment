import React, { Component } from 'react';
import swapiModule from '../../utils/swapiModule'
import { connect } from 'react-redux';
import {login} from '../../store/actions/authActions' 

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
    	username: "",
    	password: "",
    	processing: false,
    	errorMsg: null
    }
   }
  updateKey(e) {
	 let {target} = e;
	 this.setState({[target.name]: target.value});
  }

  authenticate(e) {
  	e.preventDefault();
  	this.setState({errorMsg: '', processing: true})
  	swapiModule.getPeople((res) => {
  		if(res.results && res.results.length) {	
  			this.searchUser(res.results);
  		} else {
  			this.setFailedMsg('Something went wrong.')
  		}
  		console.log(res)
  	})
  }

  searchUser(list) {
  	let {username, password} = this.state;
  	let found = list.filter(user => (user.name === username && user.birth_year === password))
 		if(found.length) {
 			this.props.dispatch(login(true, found[0]));
 			this.props.history.replace('/')
 		} else {
 			this.setFailedMsg('Username and password combination incorrect!')
 		}
  }

  setFailedMsg(msg) {
  	this.setState({errorMsg: msg, processing: false})
  }

  render() {
  	let {username, password, processing, errorMsg} = this.state;
    return (
      <div className="login-cont center-cont">  
      	<form onSubmit={this.authenticate.bind(this)}>
          <h2 className="header-text"> LOGIN </h2>
      		<div className="flex">
      			<input name="username" type="text" onChange={this.updateKey.bind(this)} value={username} placeholder="Username: Luke Skywalker" required/>
      		</div>
      		<div className="flex">
      			<input name="password" type="password" onChange={this.updateKey.bind(this)} value={password} placeholder="Password: 19BBY" required/>
      		</div>
      		{errorMsg && <div className="flex error-msg"> {errorMsg} </div>}
      		<button className="button" disabled={processing} type="submit"> Log In </button>
      	</form>
      </div>
    );
  }
}


export default connect()(Login);
