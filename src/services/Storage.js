
class Storage {

  get(name) {
    try {
      return JSON.parse(localStorage.getItem(name)) || {}
    } catch(e) {
      return {};
    }
  }

  set(name, data) {
    localStorage.setItem(name, JSON.stringify(data));
  }

  saveAuth(data) {
    this.set('auth_user', data);
  }

  getAuth() {
    return this.get('auth_user');
  }

  getUser() {
    return this.get('auth_user').user;
  }

  logout() {
    localStorage.removeItem('auth_user')
  }
}

export default new Storage();
