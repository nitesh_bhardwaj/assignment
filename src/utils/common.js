exports.checkTimePass = (time) => {
  let lastTime = new Date(time);
  lastTime.setMinutes(lastTime.getMinutes() + 1);
  return (lastTime < new Date());
}