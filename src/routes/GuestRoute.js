import React from 'react';	
import {Route, Redirect} from 'react-router-dom'
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
});

const GuestRoute = (props) => {
  let {component, ...others} = props;
  return (<Route {...others} render={() => (
    props.isLoggedIn? (
      <Redirect to="/"/>
    ) : (
      <Route {...others} component={component} />
    )
  )}/>)
}

export default connect(mapStateToProps, {})(GuestRoute)