import React from 'react';
import {Route, Redirect, withRouter} from 'react-router-dom'
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  isLoggedIn: state.auth.isLoggedIn,
});

const AuthRouter = (props) => {

  let {component, ...others} = props;
  return (<Route {...others} render={() => {
    if(!props.isLoggedIn) {
      return (<Redirect to={{pathname:"/login", search: `?back=${props.location.pathname}` }} />);
    }
    return (<Route {...others} component={component} />)
  }
  } />)

}
export default connect(mapStateToProps, {})(withRouter(AuthRouter))
