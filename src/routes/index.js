import React, { Component } from 'react';
import {Route, Switch} from 'react-router-dom'
import AuthRoute from './AuthRoute'
import GuestRoute from './GuestRoute'
import Home from '../pages/home'
import Login from '../pages/login'
import NotFound from '../pages/404'

class RouteList extends Component {
  render() {
    return (
    <div>
      <Switch>
       	<AuthRoute exact path="/" component={Home}/>
        <GuestRoute exact path="/login" component={Login}/>
        <Route component={NotFound} />
      </Switch>
     </div>
    );
  }
}

export default RouteList;
