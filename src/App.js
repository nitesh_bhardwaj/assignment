import React, { Component } from 'react';
import RouteList from './routes'
import { BrowserRouter as Router } from 'react-router-dom'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import  './App.css'
import reducers from './store/reducers'

let store = createStore(reducers);

class App extends Component {

  render() {
    return (
    <Provider store={store}>
     <Router>
      <RouteList />
     </Router>
    </Provider>
    );
  }
}

export default App;
